<?php

namespace App\Http\Controllers\Api;

use App\Api\ApiMessages;
use App\Http\Controllers\Controller;
use Throwable;

class SendMessageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function message()
    {
        try {
            $data_send = [
                'user_id' => 111,
                'supervisor_id' => 222,
                'drawer_id' => 333,
                'pin' => 123456,
                'date_in' => '2021-12-22 17:43:01',
                'protocol' => 'BEE-2107202111411-ZIONS-0e12831b',
            ];

            return response()->json([
                'test' => 'Ok!',
                'data' => $data_send
            ]);
        } catch (Throwable $e) {
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 401);
        }
    }


}
