<?php

namespace App\Http\Controllers\Api;

use App\Api\ApiMessages;
use App\Http\Controllers\Controller;
use App\Http\Resources\TenantCollection;
use App\Http\Resources\TenantResource;
use App\Models\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Throwable;

class ApiTenantController extends Controller
{
    /**
     * @param Tenant $tenant
     */
    private $tenant;

    public function __construct(Tenant $tenant)
    {
        $this->tenant = $tenant;
    }

    /**
     * Display a listing of the resource.
     *
     * @return TenantCollection
     */
    public function index(Request $request)
    {
        $objects = $this->tenant;

        $objectRepository = new TenantResource($objects);

        if($request->has('coditions')) {
            $objectRepository->selectCoditions($request->get('coditions'));
        }

        if($request->has('fields')) {
            $objectRepository->selectFilter($request->get('fields'));
        }

//        return new TenantCollection($objectRepository->paginate(10));
        return new TenantCollection($objectRepository->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required','max:127'],
            'description' => ['required','max:255'],
        ]);

        if ($validator->fails())
        {
            $message = new ApiMessages($validator->errors());
            return response()->json($message->getMessage(), 406);
        }
        $validated = $validator->validated();

        try {
            $this->tenant->create($validated);

            return response()->json([
                'data' => [
                    'msg' => 'Register created Successfully!'
                ]
            ]);
        } catch (Throwable $e) {
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
//        $tenant = auth('api')->user()->tenant;
//
//        return response()->json([
//            'data' => $tenant
//        ]);
        try {
            $object = $this->tenant->findOrFail($id);

            $filtered = new TenantResource($object);

            return response()->json([
                'data' => $filtered
            ]);

        } catch (Throwable $e) {
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['updated_at'] = now();

        try {
            $this->tenant->findOrFail($id)->update($data);

            return response()->json([
                'data' => [
                    'msg' => 'Register updated successfully!'
                ]
            ]);

        } catch (Throwable $e) {
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->tenant->findOrFail($id)->delete();

            return response()->json([
                'data' => [
                    'msg' => 'Register ' . $id . ' deleted successfully!',
                ]
            ]);
        } catch (Throwable $e) {
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 400);
        }
    }
}
