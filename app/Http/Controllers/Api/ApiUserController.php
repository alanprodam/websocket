<?php

namespace App\Http\Controllers\Api;

use App\Api\ApiMessages;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rules\Password;
use Throwable;

class ApiUserController extends Controller
{
    /**
     * @param User $user
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return UserCollection
     */
    public function index()
    {
        $tenant_users = auth('api')->user()->tenant;
        $objects = $tenant_users->users()->get();

        $objectRepository = new UserResource($objects);

        return new UserCollection($objectRepository->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required','max:255'],
            'email' => ['required','email'],
            'password' => ['required', 'confirmed', Password::min(6)->numbers()],
            'phone' => ['required','digits:13'], //digits_between:min,max
        ]);

        if ($validator->fails())
        {
            $message = new ApiMessages($validator->errors());
            return response()->json($message->getMessage(), 406);
        }
        $validated = $validator->validated();

        try {
            $tenant = auth('api')->user()->tenant;
            $validated['tenant_id'] = $tenant->id;
            $count_same = count($tenant->users()->where('email', $validated['email'])->get());

            if($count_same > 0)
            {
                $message = new ApiMessages('Email already registered in Tenant!');
                return response()->json($message->getMessage(), 406);
            }

            $validated['password'] = bcrypt($validated['password']);
            $validated['remember_token'] = Hash::make(random_int(100000,999999));
            $validated['email_verified_at'] = now();
            $validated['permission'] = 0;

        } catch (Throwable $e) {
            $message = new ApiMessages('Error user validate email');
            return response()->json($message->getMessage(), 400);
        }

        try {
            $object = $this->user->create($validated);
            $response_web = response()->json($object);

            if($response_web->isSuccessful()) {
                return response()->json([
                    'data' => [
                        'msg' => 'Register created successfully!',
                    ]
                ]);
            }
            else {
                $message = new ApiMessages('Error to register in Web!');
                return response()->json($message->getMessage(), 400);
            }
        } catch (Throwable $e) {
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $tenant_user = auth('api')->user()->tenant;

            $object = $tenant_user->users()->findOrFail($id);

            $filtered = new UserResource($object);

            return response()->json([
                'data' => $filtered
            ]);
        } catch (Throwable $e) {
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            $tenant = auth('api')->user()->tenant;

            if($request->has('password') && $request->get('password'))
            {
                $data['password'] = bcrypt($data['password']);
            } else {
                unset($data['password']);
            }

            $name_request = $tenant->users()->findOrFail($id)->name;

            if($request->has('name') && $request->get('name') && $data['name'] != $name_request)
            {
                $count_same = count($tenant->users()->where('name', $data['name'])->get());

                if($count_same > 0)
                {
                    $message = new ApiMessages('Name already registered in Tenant!');
                    return response()->json($message->getMessage(), 406);
                }
            }

        } catch (Throwable $e) {
            $message = new ApiMessages('Error user validate in name update');
            return response()->json($message->getMessage(), 400);
        }

        try {
            $data['updated_at'] = now();
            $data['tenant_id'] = $tenant->id;

            $object = $tenant->users()->findOrFail($id)->update($data);
            $response_web = response()->json($object);

            if($response_web->isSuccessful())
            {
                return response()->json([
                    'data' => [
                        'msg' => 'Register update successfully!',
                    ]
                ]);

            }
            else {
                $message = new ApiMessages('Error to update in Web!');
                return response()->json($message->getMessage(), 400);
            }

        } catch (Throwable $e) {
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $tenant = auth('api')->user()->tenant;
            $object = $tenant->users()->findOrFail($id)->delete();
            $response_web = response()->json($object);

            if($response_web->isSuccessful())
            {
                return response()->json([
                    'data' => [
                        'msg' => 'Register ' . $id . ' deleted successfully!'
                    ]
                ]);
            }
            else{
                $message = new ApiMessages('Error to delete in Web!');
                return response()->json($message->getMessage(), 400);
            }

        } catch (Throwable $e) {
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 400);
        }
    }
}
