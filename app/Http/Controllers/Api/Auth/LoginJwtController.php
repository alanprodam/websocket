<?php

namespace App\Http\Controllers\Api\Auth;

use App\Api\ApiMessages;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoginJwtController extends Controller
{
    public function login(Request $request)
    {

        $credentials = Validator::make($request->all(), [
            'email' => 'required|email|string',
            'password' => 'required|string|numeric',
        ]);

        if ($credentials->fails())
        {
            $message = new ApiMessages($credentials->errors());
            return response()->json($message->getMessage(), 406);
        }

        $validated = $credentials->validated();

        if(!$token = auth('api')->attempt($validated))
        {
            $message = new ApiMessages('Unauthorized!');
            return response()->json($message->getMessage(), 401);
        }

        $user = auth('api')->user();

        return response()->json([
            'token' => $token,
            'name' => $user['name'],
            'permission' => $user['permission']
        ]);
    }

    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Logout Successfully!'], 200);
    }

    public function refresh()
    {
        $user = auth('api')->user();

        $token = auth('api')->refresh();

        return response()->json([
            'token' => $token,
            'name' => $user['name'],
            'permission' => $user['permission']
        ]);
    }

}
