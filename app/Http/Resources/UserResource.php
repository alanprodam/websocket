<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'permission' => $this->permission,
            'tenant' => [
                'id' => $this->tenant->id,
                'name' => $this->tenant->name,
                'description' => $this->tenant->description,
            ],
        ];
    }
}
