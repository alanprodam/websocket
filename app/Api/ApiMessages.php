<?php

namespace App\Api;

class ApiMessages
{
    private $message = [];

    public function __construct($data = [])
    {
        $this->message['errors'] = $data;
    }

    public function getMessage()
    {
        return $this->message;
    }

}
