<?php

use App\Http\Controllers\Api\ApiTenantController;
use App\Http\Controllers\Api\ApiUserController;
use App\Http\Controllers\Api\Auth\LoginJwtController;
use App\Http\Controllers\Api\SendMessageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route
Route::get('/apitest', [SendMessageController::class,'message']);

Route::post('/login', [LoginJwtController::class, 'login'])->name('login');
Route::get('/logout', [LoginJwtController::class, 'logout'])->name('logout');
Route::post('/refresh', [LoginJwtController::class, 'refresh'])->name('refresh');

Route::group(['middleware' => ['jwt.auth']], function (){

    //Route Tenant
    Route::resource('tenant', ApiTenantController::class);

    //Route User
    Route::resource('user', ApiUserController::class);

});
