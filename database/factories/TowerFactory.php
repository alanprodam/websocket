<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tower;
use Faker\Generator as Faker;

$factory->define(Tower::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'tenant_id' => $faker->numberBetween(1,3)
    ];
});


