<?php

namespace Database\Seeders;


use App\Rabbitmq\DeleteRabbitmq;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class TenantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         DB::table('tenant')->insert(
            [
                'name' => 'Zions',
                'description' => 'Localização do cliente Zions',
                'created_at' => now(),
                'updated_at' => now()
            ]
        );

        DB::table('tenant')->insert(
            [
                'name' => 'Letmein',
                'description' => 'Localização do cliente Letmein',
                'created_at' => now(),
                'updated_at' => now()
            ]
        );
    }
}
