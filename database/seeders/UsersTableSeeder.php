<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Psy\Util\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('user')->insert(
            [
                'name' => 'Porteiro Zions',
                'email' => 'porteiro@zionstech.com',
                'email_verified_at' => now(),
                'password' => bcrypt('000000'), // password
                'phone' => '5519984149656',
                'permission' => 0,
                'tenant_id' => 1,
                'remember_token' => Hash::make(000000),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );

        DB::table('user')->insert(
            [
                'name' => 'César Henrique',
                'email' => 'cesar@zionstech.com',
                'email_verified_at' => now(),
                'password' => bcrypt('111111'), // password
                'phone' => '5519971002325',
                'permission' => 1,
                'tenant_id' => 1,
                'remember_token' => Hash::make(111111),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );

        DB::table('user')->insert(
            [
                'name' => 'Alan T.',
                'email' => 'alan@zionstech.com',
                'email_verified_at' => now(),
                'password' => bcrypt('222222'), // password
                'phone' => '5519984149656',
                'permission' => 2,
                'tenant_id' => 1,
                'remember_token' => Hash::make(222222),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );

        DB::table('user')->insert(
            [
                'name' => 'Randerson',
                'email' => 'randerson@zionstech.com',
                'email_verified_at' => now(),
                'password' => bcrypt('333333'), // password
                'phone' => '5519982593960',
                'permission' => 2,
                'tenant_id' => 1,
                'remember_token' => Hash::make(333333),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );

        DB::table('user')->insert(
            [
                'name' => 'Alexandre R.',
                'email' => 'alexandre@zionstech.com',
                'email_verified_at' => now(),
                'password' => bcrypt('444444'), // password
                'phone' => '556281632402',
                'permission' => 2,
                'tenant_id' => 1,
                'remember_token' => Hash::make(444444),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );

        DB::table('user')->insert(
            [
                'name' => 'Rafael',
                'email' => 'rafa@zionstech.com',
                'email_verified_at' => now(),
                'password' => bcrypt('555555'), // password
                'phone' => '5519993990316',
                'permission' => 2,
                'tenant_id' => 1,
                'remember_token' => Hash::make(555555),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );
    }
}
